<?php

namespace Drupal\media_default_image\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Media default image settings for this site.
 */
class DefaultImageSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_default_image_default_image_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['media_default_image.settings'];
  }

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected FileUsageInterface $fileUsage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->fileRepository = $container->get('file.repository');
    $instance->fileUsage = $container->get('file.usage');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $scheme = $this->config('system.file')->get('default_scheme');
    // Select default image.
    $form['default_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Default image'),
      '#upload_location' => $scheme . '://media_default_image/',
      '#multiple' => FALSE,
      '#description' => $this->t('Default image to replace if not found image'),
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      '#default_value' => $this->config('media_default_image.settings')->get('default_image'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $default_image = $form_state->getValue(['default_image']);
    if (empty($default_image)) {
      $form_state->setErrorByName('test', 'No image found for image one');
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $image = $form_state->getValue('default_image');
    $config = $this->config('media_default_image.settings');
    if (!empty($image) && $config->get('default_image') !== $image) {
      try {
        $file = $this->entityTypeManager->getStorage('file')->load($image[0]);
        if ($file instanceof FileInterface) {
          $file_contents = file_get_contents($file->getFileUri());
          $this->fileRepository->writeData($file_contents, $file->getFileUri(), FileSystemInterface::EXISTS_RENAME);
          $this->fileUsage->add($file, 'media_default_image', 'module', 'default_image_formatter');
          $file->setPermanent();
          $file->save();
        }
      }
      catch (FileException $exception) {
        $this->logger('media_default_image')->error($exception->getMessage());
      }

      $config->set('default_image', $form_state->getValue('default_image'))->save();
    }
    parent::submitForm($form, $form_state);
  }

}
