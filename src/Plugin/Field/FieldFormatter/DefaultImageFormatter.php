<?php

namespace Drupal\media_default_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\Plugin\Field\FieldFormatter\MediaThumbnailFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Default Image' formatter.
 *
 * @FieldFormatter(
 *   id = "media_default_image_default_image",
 *   label = @Translation("Default Image"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class DefaultImageFormatter extends MediaThumbnailFormatter {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * Config module name.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * System file configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $systemFileConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory');
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as &$e) {
      if (isset($e['#build'])) {
        $e['#build']['content'] = [];
      }
    }

    unset($e);

    if (empty($elements)) {
      $entity = $items->getEntity();
      $settings = parent::getSettings();
      $delta = 0;
      if (array_key_exists('image_style', $settings) && !empty($settings['image_style'])) {
        $image_style = $settings['image_style'];
      }
      else {
        return [];
      }

      try {
        $file = $this->getDefaultFile();

        if ($file instanceof FileInterface) {
          $image_uri = $file->getFileUri();
          $elements[$delta] = [
            '#theme' => 'image_style',
            '#style_name' => $image_style,
            '#uri' => $image_uri,
            '#alt' => $entity->label(),
          ];
        }
      }
      catch (\Exception $exception) {
        $this->logger->get('media_default_image')
          ->error($exception->getMessage());
      }

      if ($this->getSetting('image_link')) {
        $elements[$delta] = [
          '#type' => 'link',
          '#url' => $this->getContentUrl($entity),
          '#title' => $elements[$delta],
        ];
      }
    }

    return $elements;
  }

  /**
   * Get the URL for the media thumbnail.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that the field belongs to.
   *
   * @return \Drupal\Core\Url|null
   *   The URL object for the media item or null if we don't want to add
   *   a link.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getContentUrl(EntityInterface $entity) {
    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting === 'content') {
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    return $url;
  }

  /**
   * Get default image file.
   */
  protected function getDefaultFile() {
    $fid = $this->config('media_default_image.settings')
      ->get('default_image');
    // If file is not exist get from entity.
    if (empty($fid)) {
      $default_scheme = $this->config('system.file')->get('default_scheme');
      $entity = $this->entityTypeManager
        ->getStorage('file')
        ->loadByProperties(['uri' => $default_scheme . '://media_default_image/image_default.jpg']);
      $file = reset($entity);
    }
    else {
      $file = File::load($fid[0]);
    }

    return $file;
  }

  /**
   * Get Configuration Factory.
   *
   * @param string $variable
   *   The configuration data.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   System file configuration.
   */
  private function config(string $variable) {
    return $this->configFactory->get($variable);
  }

}
