# Media Default Image

This module add widget media that provide default image if it is not found.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_default_image).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_default_image).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. When enabled, the module will set default value input
of image on each field image.


## Maintainers

- Khalil Charfi - [ckhalilo](https://www.drupal.org/u/ckhalilo)